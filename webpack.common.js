const QuickPaperLoaderPlugin = require('quick-paper/loader-plug/index.js');

module.exports = {
    entry: ['./src/entry.js'],
    output: {
        path: __dirname,
        filename: 'dist/debug-phone.min.js'
    },
    resolve: {
        alias: {
            'quick-paper': 'quick-paper/dist/quick-paper.core.min.js'
        }
    },
    module: {
        rules: [{
            test: /\.paper$/,
            loader: ['quick-paper/loader/index.js'],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            loader: ['./quickpaper-style-loader/index.js', 'css-loader', 'postcss-loader']
        }, {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.(png|jpg|jpeg|gif|bmp)$/,
            loader: [{
                loader: "url-loader",
                options: {
                    name: "build/[name].[ext]",
                    context: "src/asset",
                    limit: 500000000
                }
            }]
        }]
    },
    plugins: [
        new QuickPaperLoaderPlugin()
    ]
};
